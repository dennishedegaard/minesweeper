$(function() {
    var div = $('div.minesweeper');
    var minesweeper = undefined;
    var width = 9;
    var height = 9;
    var mines = 10;

    function generate_minesweeper_ui(width, height, mines) {
        minesweeper = new Minefield(width, height, mines);
        redraw_gui();
    };

    function redraw_gui() {
        div.empty();
        var buttons = '';
        for (var y = 0; y < height; y++) {
            var row = '';
            row += '<div class="row">';
            for (var x = 0; x < width; x++) {
                if (minesweeper.is_cleared(x, y)) {
                    var adjacent = minesweeper.get_n_adjacent_mines(x, y);
                    if (adjacent === 0) {
                        adjacent = '';
                    }
                    row += '<button class="btn disabled" x="' + x + '" y="' + y + '">' + adjacent + '</button>';
                } else {
                    row += '<button class="btn btn-primary" x="' + x + '" y="' + y + '">';
                    var flag = minesweeper.get_flag(x, y)
                    if (flag === FlagType.FLAG) {
                        row += '<i class="icon-white icon-flag"></i>';
                    } else if (flag === FlagType.MAYBE) {
                        row += '<i class="icon-white icon-question-sign"></i>';
                    }
                    row += '</button>';
                }
            }
            row += '</div>';
            buttons += row;
        }
        div.html(buttons);

        var buttons = $('button');
        var non_cleared = $('button.btn-primary');

        buttons.bind('contextmenu', function(event) {
            return false;
        });

        non_cleared.click(function() {
            var x = $(this).attr('x');
            var y = $(this).attr('y');

            minesweeper.clear_mine(x, y);
            redraw_gui();

            console.log('x: ' + x + ' - y: ' + y);
        });

        non_cleared.mousedown(function(event) {
            if (event.which === 3) {
                var elem = $(this);
                var x = elem.attr('x');
                var y = elem.attr('y');
                event.preventDefault();

                var flag = minesweeper.get_flag(x, y);

                switch (flag) {
                case FlagType.NONE:
                    minesweeper.set_flag(x, y, FlagType.FLAG);
                    break;
                case FlagType.FLAG:
                    minesweeper.set_flag(x, y, FlagType.MAYBE);
                    break;
                case FlagType.MAYBE:
                    minesweeper.set_flag(x, y, FlagType.NONE);
                    break;
                }
                redraw_gui();

                return true;
            };
        });
    }
    generate_minesweeper_ui(9, 9, 10);
});
