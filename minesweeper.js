var FlagType = {
    NONE: 1,
    FLAG: 2,
    MAYBE: 3
};

function Location() {
    "use strict";

    this.has_mine = false;
    this.cleared = false;
    this.flag = FlagType.NONE;

    this.toString = function() {
        return 'hej';
    };
}

function Neighbor(x, y) {
    "use strict";

    this.x = x;
    this.y = y;
}

var neighbor_map = [
    new Neighbor(-1, 1),
    new Neighbor(0, 1),
    new Neighbor(1, 1),
    new Neighbor(1, 0),
    new Neighbor(1, -1),
    new Neighbor(0, -1),
    new Neighbor(-1, -1),
    new Neighbor(-1, 0)
];

function Minefield(width, height, n_mines) {
    "use strict";

    this.width = 0;
    this.height = 0;
    this.n_mines = 0;
    this.locations = undefined;
    this.exploded = false;
    this.placed_mines = false;
    this.n_cleared = 0;
    this.n_flags = 0;

    this.ctor = function(width, height, n_mines) {
        this.width = width;
        this.height = height;
        this.n_mines = n_mines;

        this.locations = [];
        for (var y = 0;y < height;y++) {
            var row = [];
            for (var x = 0; x < width;x++) {
                row.push(new Location());
            }
            this.locations.push(row);
        }
    };

    this.is_complete = function() {
        return this.n_cleared === this.width * this.height - this.n_mines;
    };

    this.has_mine = function(x, y) {
        return this.locations[y][x].has_mine;
    };

    this.is_location = function(x, y) {
        return x >= 0 && y >= 0 && x < this.width && y < this.height;
    };

    this.is_cleared = function(x, y) {
        return this.locations[y][x].cleared;
    };

    this.clear_mine = function(x, y) {
        if (!this.placed_mines) {
            this.place_mines(x, y);
            this.placed_mines = true;
        }

        if (this.locations[y][x].cleared || this.locations[y][x].flag === FlagType.FLAG) {
            return;
        }

        this.clear_mines_recursive(x, y);

        if (this.locations[y][x].has_mine) {
            if (!this.exploded) {
                this.exploded = true;
                this.explode();
            }
            return;
        }

        if (this.is_complete) {
            for (var tx = 0; tx < this.width; tx++) {
                for (var ty = 0; ty < this.height; ty++) {
                    if (this.has_mine(tx, ty) ) {
                        this.set_flag(tx, ty, FlagType.FLAG);
                    }
                }
            }
            this.cleared();
        }
    };

    this.clear_mines_recursive = function(x, y) {
        if (this.locations[y][x].cleared) {
            return;
        }

        this.locations[y][x].cleared = true;
        this.n_cleared++;
        if (this.locations[y][x].flag === FlagType.FLAG) {
            this.n_flags--;
        }
        this.locations[y][x].flag = FlagType.NONE;
        this.redraw_sector(x, y);
        this.marks_changed();

        if (!this.locations[y][x].has_mine && this.get_n_adjacent_mines(x, y) === 0) {
            for (var i in neighbor_map) {
                if (i !== undefined) {
                    var neighbor = neighbor_map[i];
                    var nx = x + neighbor.x;
                    var ny = y + neighbor.y;
                    if (this.is_location(nx, ny)) {
                        this.clear_mines_recursive(nx, ny);
                    }
                }
            }
        }
    };

    this.set_flag = function(x, y, flag) {
        if (this.locations[y][x].cleared || this.locations[y][x].flag === flag) {
            return;
        }

        if (flag === FlagType.FLAG) {
            this.n_flags++;
        } else if (this.locations[y][x].flag === FlagType.FLAG) {
            this.n_flags--;
        }

        this.locations[y][x].flag = flag;
        this.redraw_sector(x, y);

        for (var i in neighbor_map) {
            if (i !== undefined) {
                var neighbor = neighbor_map[i];
                var nx = x + neighbor.x;
                var ny = y + neighbor.y;
                if (this.is_location(nx, ny) && this.is_cleared(nx, ny)) {
                    this.redraw_sector(nx, ny);
                }
            }
        }

        this.marks_changed();
    };

    this.get_flag = function(x, y) {
        return this.locations[y][x].flag;
    };

    this.hint = function() {
        throw new Error('NYI');
    };

    this.get_n_adjacent_mines = function(x, y) {
        var n = 0;
        for (var i in neighbor_map) {
            if (i !== undefined) {
                var neighbor = neighbor_map[i];
                var nx = x + neighbor.x;
                var ny = y + neighbor.y;
                if (this.is_location(nx, ny) && this.has_mine(nx, ny)) {
                    n++;
                }
            }
        }
        return n;
    };

    this.has_flag_warning = function(x, y) {
        if (!this.is_cleared(x, y)) {
            return false;
        }

        var _n_mines = 0;
        var _n_flags = 0;
        for (var i in neighbor_map) {
            if (i !== undefined) {
                var neighbor = neighbor_map[i];
                var nx = x + neighbor.x;
                var ny = y + neighbor.y;
                if (!this.is_location(nx, ny)) {
                    continue;
                }
                if (this.has_mine(nx, ny)) {
                    _n_mines++;
                }
                if (this.get_flag(nx, ny) === FlagType.FLAG) {
                    _n_flags++;
                }
            }
        }

        return _n_flags > _n_mines;
    };

    this.place_mines = function(x, y) {
        for (var n = 0; n < this.n_mines;) {
            var rx = Math.floor(Math.random() * this.width);
            var ry = Math.floor(Math.random() * this.height);

            if (rx === x && ry === y) {
                continue;
            }

            if (!this.locations[ry][rx].has_mine) {
                var adj_found = false;

                for (var i in neighbor_map) {
                    if (i !== undefined) {
                        var neighbor = neighbor_map[i];
                        if (rx === x + neighbor.x && ry === y + neighbor.y) {
                            adj_found = true;
                            break;
                        }
                    }
                }

                if (!adj_found) {
                    this.locations[ry][rx].has_mine = true;
                    n++;
                }
            }
        }
    };

    this.redraw_sector = function() {
        throw new Error('please override me!');
    };

    this.marks_changed = function() {
        throw new Error('please override me!');
    };

    this.explode = function() {
        throw new Error('please override me!');
    };

    this.cleared = function() {
        throw new Error('please override me!');
    };

    this.ctor(width, height, n_mines);
}
